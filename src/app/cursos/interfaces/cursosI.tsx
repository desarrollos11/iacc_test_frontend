interface CursosI {
    columns: ColumnsI[];
    dataSource: DataSourceI[];
}

interface ColumnsI {
    title: string;
    dataIndex: string;
    key: string;
}
  
interface DataSourceI {
    key: string;
    name: string;
    age: number,
    address: string;
}
interface CursoI {
    id: number;
    name: string;
    codigo: string;
    anio: string;
    semestre: string;
    sede: string;
}
interface Props {
cursos: CursoI[]
}