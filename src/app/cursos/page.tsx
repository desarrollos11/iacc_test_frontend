import { NextPage } from "next";
import React from "react"
import { TableComponent } from "../components/TableComponent";
import BreadcrumbComponent from "../components/BreadcrumbComponent";



const Home: NextPage<Props> = (props) => {
  return(
      <div style={{padding:35}}>

        <div style={{padding:35}}>
          <BreadcrumbComponent items={[{title:"Inicio"},{title:"Cursos"}]}></BreadcrumbComponent>
        </div>
        <div style={{padding:35}}>
          <TableComponent data={props.cursos}></TableComponent>
        </div>

      </div>

  )
}
export async function getServerSideProps() {
  try {
    const res = await fetch("http://localhost:3000/courses");
    const data: CursoI[] = await res.json();
    return {
      props: {
        cursos:data
      }
  }
  } catch (error) {
    console.error("Error al obtener los datos de la API:", error);

    return {
      props: {
        response:""
      },
    };
  }
}
export default Home;


