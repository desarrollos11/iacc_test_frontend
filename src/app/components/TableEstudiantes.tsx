"use client"
import React from 'react'
import {Table} from 'antd'

interface TableComponentI {
  data: EstudiantesI[];
}
export const TableEstudiantes = ({data}:TableComponentI) => {
  
  //POR ALGUNA RAZON data SE MUESTRA COMO UNDEFINED, **PARCIALMENTE HE DEJADO DATA DE PRUEBA**. 
  //  console.log(data)
  //SI ES POSIBLE RECIBIR AYUDA DEL EQUIPO PARA ENCONTRAR UNA SOLUCION ESTARIA MUY AGRADECIDO.

  const dataSource = [
      {
        id: 1,
        nombre: 'Arturo',
        apellidoPaterno: 'Vidal',
        apellidoMaterno: 'Rodriguez',
        email: 'RDSII',
        edad: 2,
        direccion: 'RDSII',
      },
      {
        id: 2,
        nombre: 'Arturo',
        apellidoPaterno: 'Rodriguez',
        apellidoMaterno: 'Vidal',
        email: 'RDSII',
        edad: 2,
        direccion: 'RDSII',
      },
      {
        id: 3,
        nombre: 'Arturo Vidal',
        apellidoPaterno: 'RDSII',
        apellidoMaterno: 'RDSII',
        email: 'RDSII',
        edad: 2,
        direccion: 'RDSII',
      },
      {
        id: 4,
        nombre: 'Arturo Vidal',
        apellidoPaterno: 'RDSII',
        apellidoMaterno: 'RDSII',
        email: 'RDSII',
        edad: 2,
        direccion: 'RDSII',
      },
      {
        id: 5,
        nombre: 'Arturo Vidal',
        apellidoPaterno: 'RDSII',
        apellidoMaterno: 'RDSII',
        email: 'RDSII',
        edad: 2,
        direccion: 'RDSII',
      },
      {
        id: 6,
        nombre: 'Arturo Vidal',
        apellidoPaterno: 'RDSII',
        apellidoMaterno: 'RDSII',
        email: 'RDSII',
        edad: 2,
        direccion: 'RDSII',
      },
    ];
    
    const columns = [
      {
        title: 'Nombre',
        dataIndex: 'nombre',
        key: 'nombre',
      },
      {
        title: 'Apellido Paterno',
        dataIndex: 'apellidoPaterno',
        key: 'apellidoPaterno',
      },
      {
        title: 'Apellido Materno',
        dataIndex: 'apellidoMaterno',
        key: 'apellidoMaterno',
      },
      {
        title: 'email',
        dataIndex: 'email',
        key: 'email',
      },
      {
        title: 'edad',
        dataIndex: 'edad',
        key: 'edad',
      },
      {
        title: 'direccion',
        dataIndex: 'direccion',
        key: 'direccion',
      },
    ];
      
  return (
    <Table dataSource={dataSource} columns={columns} />
  )
}
