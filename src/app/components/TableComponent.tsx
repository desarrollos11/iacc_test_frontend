"use client"
import React from 'react'
import {Table} from 'antd'
interface Curso {
  id: number;
  nombre: string;
  codigo: string;
  anio: number;
  semestre: string;
  sede: string;
}

interface TableComponentI {
  data: CursoI[];
}
export const TableComponent = ({data}:TableComponentI) => {
  
  //POR ALGUNA RAZON data SE MUESTRA COMO UNDEFINED, **PARCIALMENTE HE DEJADO DATA DE PRUEBA**. 
  //  console.log(data)
  //SI ES POSIBLE RECIBIR AYUDA DEL EQUIPO PARA ENCONTRAR UNA SOLUCION ESTARIA MUY AGRADECIDO.

  const dataSource = [
      {
        id: 1,
        nombre: 'Programacion I',
        codigo: 'PGRI',
        anio: 2022,
        semestre: "1",
        sede: 'Santiago',
      },
      {
        id: 2,
        nombre: 'Programacion II',
        codigo: 'PGRII',
        anio: 2022,
        semestre: "1",
        sede: 'Santiago',
      },
      {
        id: 3,
        nombre: 'Matematicas I',
        codigo: 'MTM1',
        anio: 2022,
        semestre: "1",
        sede: 'Santiago',
      },
      {
        id: 4,
        nombre: 'Matematicas II',
        codigo: 'MTMII',
        anio: 2022,
        semestre: "1",
        sede: 'Inglaterra',
      },
      {
        id: 5,
        nombre: 'Redes I',
        codigo: 'RDSI',
        anio: 2022,
        semestre: "1",
        sede: 'Santiago',
      },
      {
        id: 6,
        nombre: 'Redes II',
        codigo: 'RDSII',
        anio: 2022,
        semestre: "1",
        sede: 'Santiago',
      },
    ];
    
    const columns = [
      {
        title: 'Nombre',
        dataIndex: 'nombre',
        key: 'nombre',
        render: (text: string, record: Curso) => (
          <h5 className="title">
            {/*<a href={"/students/"+record.id}>{text}</a>*/}
            <a href={"/estudiantes/"}>{text}</a>
          </h5>
        ),
      },
      {
        title: 'Código',
        dataIndex: 'codigo',
        key: 'codigo',
      },
      {
        title: 'Año',
        dataIndex: 'anio',
        key: 'anio',
      },
      {
        title: 'Semestre',
        dataIndex: 'semestre',
        key: 'semestre',
      },
      {
        title: 'Sede',
        dataIndex: 'sede',
        key: 'sede',
      },
    ];
      
  return (
    <Table dataSource={dataSource} columns={columns} />
  )
}
