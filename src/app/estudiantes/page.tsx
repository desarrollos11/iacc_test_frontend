
import { NextPage } from "next";
import React from "react"
import { TableComponent } from "../components/TableComponent";
import BreadcrumbComponent from "../components/BreadcrumbComponent";
import { TableEstudiantes } from "../components/TableEstudiantes";



const Home: NextPage<Props> = (props) => {
  return(
      <div style={{padding:35}}>

        <div style={{padding:35}}>
          <BreadcrumbComponent items={[{title:"Inicio"},{title:"Estudiantes"}]}></BreadcrumbComponent>
        </div>
        <div style={{padding:35}}>
          <TableEstudiantes data={props.estudiantes}></TableEstudiantes>
        </div>

      </div>

  )
}
export async function getServerSideProps() {
  try {
    const res = await fetch("http://localhost:3000/students/");
    const data: EstudiantesI[] = await res.json();
    return {
      props: {
        estudiantes:data
      }
  }
  } catch (error) {
    console.error("Error al obtener los datos de la API:", error);

    return {
      props: {
        response:""
      },
    };
  }
}
export default Home;


