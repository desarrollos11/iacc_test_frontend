
interface EstudiantesI {
    id: number;
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    email: string;
    edad: number;
    direccion: string;

}

interface Props {
    estudiantes: EstudiantesI[]
}